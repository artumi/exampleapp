//
//  DetailViewController.swift
//  TableExample
//
//  Created by Artur Minasjan on 19/06/2017.
//  Copyright © 2017 Example. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var detailTitleLabel: UILabel!
    
    var stringPassed = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        detailTitleLabel.text = stringPassed
        
        detailImageView.frame = resizeContainer(container: detailImageView, image: UIImage(named: stringPassed)!)
        detailImageView.image = UIImage(named: stringPassed)
        
        detailTitleLabel.frame = changeLabelOrigin(container: detailImageView, label: detailTitleLabel)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func resizeContainer(container: UIImageView, image: UIImage) -> CGRect {
        var width:(CGFloat)
        var height:(CGFloat)
        width = container.frame.width
        height = image.size.height * ((width * 100)/image.size.width)*0.01
        
        let frame = CGRect(x: 0, y: 0, width: width, height: height)
        
        return frame
    }
    
    func changeLabelOrigin(container: UIImageView, label: UILabel) -> CGRect {
        let frame = CGRect(x: label.frame.origin.x, y: container.frame.size.height + 10, width: label.frame.size.width, height: label.frame.size.height)
        
        return frame
    }
}
