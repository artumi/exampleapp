import UIKit

class ListTableViewCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var pictureImageView: UIImageView!
}

class ExampleTableViewController: UITableViewController {
    
    var images = ["Landscape", "Portrait"]
    var labelValueToPass:String!
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as! ListTableViewCell

        let imageName = images[indexPath.row]
        cell.label?.text = imageName
        cell.pictureImageView?.image = UIImage(named: imageName)
        
        return cell
    }
    
    override  func tableView(_ tableView: UITableView, didSelectRowAt
        indexPath: IndexPath){
        labelValueToPass = images[indexPath.row]
        performSegue(withIdentifier: "detailSegue", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailSegue" {
            let detailVc = segue.destination as! DetailViewController
            detailVc.stringPassed = labelValueToPass
        }
    }
    
}
